from django.contrib import admin
from .models import Article,Category



class CategoryAdmin(admin.ModelAdmin):
    list_display = ['position','title','slug','status',] 
    list_filter = (['status'])
    search_fields = ('title','slug')
    prepopulated_fields = {'slug':('title',)}
    
admin.site.register(Category,CategoryAdmin)

class ArticleAdmin(admin.ModelAdmin):
    list_display = ['titie','slug','jpublish','status','category_to_str'] 
    list_filter = ['publish','status',]
    search_fields = ('titie','description')
    prepopulated_fields = {'slug':('titie',)}
    ordering = ['status' , 'publish']
    def category_to_str(self,obj):
        return ", ".join([category.title for category in obj.category_published()])
    category_to_str.short_description="دسته بندی ها"
admin.site.register(Article,ArticleAdmin)