from django.db import models
from django.utils import timezone
from extensions.utils import jalali_converter

class ArticleManager(models.Manager):
       def published(self):
              return self.filter(status = "p")

class Category(models.Model):
       parent = models.ForeignKey('self',default=None,null=True,blank=True,on_delete=models.SET_NULL,related_name="children",verbose_name='زیردسته')
       title = models.CharField(max_length=200, verbose_name="دسته بندی مقالات")
       slug=models.SlugField(max_length=200,unique=True,verbose_name="آدرس دسته بندی ها")
       status = models.BooleanField(default=True,verbose_name="آیا نمایش داده شود؟")
       position = models.IntegerField(verbose_name="موقعیت")
       
       class Meta:
         verbose_name = "دسته بندی"
         verbose_name_plural = "دسته بندی ها"
         ordering = ['parent__id','position']
         
       def __str__(self):
           return self.title

# Create your models here for Article table.
class Article(models.Model):
    STATUS_CHOICES =( 
           ('d','پیش نویس'),
           ('p','منتشر شده')           
    )
    titie = models.CharField(max_length=200 , verbose_name="عنوان مقاله")
    slug = models.SlugField(max_length=100,unique=True,verbose_name="آدرس مقاله")
    category = models.ManyToManyField(Category,verbose_name="دسته بندی",related_name="articles")
    description = models.TextField(verbose_name="توضیحات")
    thumbnail = models.ImageField(upload_to="images",verbose_name="عکس")
    publish = models.DateTimeField(default=timezone.now,verbose_name="زمان انتشار")
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=1,choices=STATUS_CHOICES,verbose_name="وضعیت")
    class Meta:
         verbose_name = "مقاله"
         verbose_name_plural = "مقالات"
         ordering = ['-publish']
         
         
    def __str__(self):  #for show the title name in the table in admin pannel
           return self.titie
    
    def jpublish(self):
           return jalali_converter(self.publish)
    jpublish.short_description ="زمان انتشار"
    
    def category_published(self):
           return self.category.filter(status = True)
    
    objects = ArticleManager() 