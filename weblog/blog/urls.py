from django.urls import path
from .views import home , detial ,category

app_name = "blog"
urlpatterns = [
    path('',home,name='home'),
    path('article/<slug:slug>',detial,name='detial'),  
    path('category/<slug:slug>',category , name="category")
     
]