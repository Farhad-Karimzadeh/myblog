from django.shortcuts import render, get_object_or_404, get_list_or_404
from .models import Article,Category
from django.core.paginator import Paginator


def home(request):
    article_list = Article.objects.published()
    paginator = Paginator(article_list, 4)
    page_number = request.GET.get('p')
    articles = paginator.get_page(page_number)
    
    context = {            # we can get data from database with model
        "articles": articles,
        # "category":  Category.objects.filter(status=True)
    }
    return render(request, "blog/home.html", context)


def detial(request, slug):
    context = {            # we can get data from database with model
        # "article":  Article.objects.get(slug=slug)
        # صفحه 404 و پستهایی که پابلیش شدند نمایش داده می شوند
        "article":  get_object_or_404(Article.objects.published(), slug=slug),
        # "category":  Category.objects.filter(status=True)
    }
    return render(request, "blog/detail.html", context)

def category(request,slug):
    context={
        "category": get_object_or_404(Category, slug=slug ,status = True )
    }
    return render(request,"blog/category.html",context)

